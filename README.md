# **List of useful hints for PostreSQL**

---

### Change column type to timestamp:
###
```
ALTER TABLE table_name ALTER COLUMN column_name SET DATA TYPE timestamp USING column_name::timestamp without time zone
```